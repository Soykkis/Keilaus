﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausTesti
{
    public class KeilausPeli
    {

        private int[] rolls = new int[21];
        private int currentRoll = 0;

        public int Score
        {
            get
            {
                int score = 0;
                int rollIndex = 0;
                for (int frame = 0; frame < 10; frame++)
                {
                    if (rolls[rollIndex] == 10)
                    {
                        score += rolls[rollIndex] + rolls[rollIndex + 1] + rolls[rollIndex + 2];
                        rollIndex++;
                    }
                    else if (rolls[rollIndex] + rolls[rollIndex + 1] == 10)
                    {
                        score += rolls[rollIndex] + rolls[rollIndex + 1] + rolls[rollIndex + 2];
                        rollIndex += 2;
                    }
                    else
                    {
                        score += rolls[rollIndex] + rolls[rollIndex + 1];
                        rollIndex += 2;
                    }
                }
                return score;
            }
        }

        public void Roll(int pins)
        {
            rolls[currentRoll++] = pins;
        }
       
    }
}