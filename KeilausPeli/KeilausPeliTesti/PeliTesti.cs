﻿using KeilausTesti;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausPeliTesti
{ 
    [TestFixture]
 
    public class PeliTestit
    { 
        public KeilausPeli peli; 
        [Test]
        public void Initialize()
        {
            peli = new KeilausPeli();

        }

       
            [Test]
            public void Nollat()
            {

                RollMany(1, 20);
                Assert.AreEqual(0, peli.Score);


            }

            [Test]
            public void YksiPiste()
            {
                RollMany(1, 20);
                Assert.AreEqual(20, peli.Score);
            }
            [Test]
            public void Paikko()
            {
                peli.Roll(7);
                peli.Roll(3);
                peli.Roll(4);
                RollMany(0, 17);
                Assert.AreEqual(18, peli.Score);
            }
            [Test]
            public void Kaato()
            {
                peli.Roll(10);
                peli.Roll(4);
                peli.Roll(5);
               RollMany(0, 16);
                Assert.AreEqual(28, peli.Score);
            }
            [Test]
            public void Täydet()
            {
                RollMany(10, 12);
                Assert.AreEqual(300, peli.Score);
            }
            [Test]
            public void Viisi()
            {
                RollMany(5, 21);
                Assert.AreEqual(150, peli.Score);
            }
            [Test]
            public void Random()
            {
                
               
                peli.Roll(1); // 3
                peli.Roll(10); // 4
                peli.Roll(3);
                peli.Roll(4);
                peli.Roll(5); // 2
                peli.Roll(9);
                peli.Roll(2); // 7
                peli.Roll(8);
                peli.Roll(1); // 5
                peli.Roll(10); // 6
                peli.Roll(9);
                peli.Roll(0); // 8
                peli.Roll(0);
                peli.Roll(8); // 9
                peli.Roll(1);
                peli.Roll(3);
                peli.Roll(10); // 1
                Assert.AreEqual(117, peli.Score);
            }

            private void RollMany(int pins, int rolls)
            {
                for (int i = 0; i < rolls; i++)
                    peli.Roll(pins);
            }
        }
    }